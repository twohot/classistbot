# Class Assistant Bot (@classistbot)

A Bot for running educative/school activities on Telegram.  With @classistbot, your group becomes an automated class channel where members can either be students, moderators or tutors(lecturers).  Class Assistant manages the class records -- sessions, quizzes and participation.  Members can track their progress and events privately by chatting with Class Assistant.

## Interacting with Class Assistant Bot
Users can interact with the bot publicly or privately.  @Classistbot will mostly reply privately unless the context is relevant to the entire group.  To talk to the bot, just issue a recognized command.  There are four public commands and four private ones.  All commands work in private conversations; However, private commands are not advertized to the group.  Certain options are possible only in private interactions.

### Public Commands
1. `/session@classistbot` or `/session` (in private chat)
2. `/quiz@classistbot` or `/quiz` (in private chat)
3. `/event@classistbot` or `/event` (in private chat)
4. `/readme@classistbot` or `/readme` (in private chat)

### Private Commands
5. `/course`
6. `/member`
7. `/progress`
8. `/announce`
9. `/bonus`
10. `/register`
11. `/whois`
12. `/set`
13. `/present`
