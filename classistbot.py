"""Free MeetBot for Telegram Main Module"""

import json
import time
import requests
from messaging import URL_PREFIX, Messenger, Message
from processing import CommandsManager
from storing import CONFIG, DBMANAGER


MESSENGER = Messenger()
COMMANDS = CommandsManager(MESSENGER)


def broker(update):
    """determine appropriate command and deliver update"""

    # TODO: Remove print statement later
    print(update)

    if "text" in update:
        # determine if update is a command or not.
        if update["text"].startswith("/"):
            message = Message(
                (
                    update["date"],
                    update["chat"]["id"],
                    update["message_id"],
                    update["from"]["id"],
                    update["chat"]["type"],
                )
            )
            COMMANDS.messenger.queue(message)
            cmdlist = update["text"].split()
            command = cmdlist[0].strip()
            args = cmdlist[1:]
            COMMANDS.messenger.message.content = args
            COMMANDS.execute(command)
    # else:
    #    COMMANDS.messenger.message.content = "Huh? Talking to me?"


class UpdatesHandler:
    """Telegram Updates Handler"""

    def __init__(self):
        """Initialize Handler"""

        self.__updates = {"last": 0, "queue": {}, "timeout": 100}
        self.__awaiting = {}

    def checkoff(self):
        """Get Bot's updates"""
        
        delay = int(CONFIG.bot()["reconnection_delay"])
        try:
            updates_log = open("log/updates.log", "a+")
            json_updates = requests.get(self.url).content.decode("utf8")
            fetched = json.loads(json_updates)["result"]
            updates_log.write(str(fetched) + "\n")
            updates_log.close()

            for update in fetched:
                if (
                    int(update["update_id"]) > self.last
                    and int(update["update_id"]) not in self.__updates["queue"]
                ):
                    update_id = int(update.pop("update_id"))
                    self.__updates["queue"][update_id] = update

            # Pull one update from collected/unhandled updates
            
            checkoff_log = open("log/checkoff.log", "a+")
            for update_id in sorted(self.__updates["queue"]):
                self.__updates["last"] = update_id
                result = self.__updates["queue"].pop(update_id)
                msgkey = list(result.keys())[0]
                checkoff_log.write(str(result[msgkey]) + "\n")
                broker(result[msgkey])
            checkoff_log.close()

        except requests.exceptions.ConnectionError:
            print(
                "Experiencing network issues. Trying again "
                + "in {} seconds".format(delay)
            )
            time.sleep(delay)

    @property
    def url(self):
        """Return the url for getting the next updates"""

        offset = ""
        url = URL_PREFIX + "getUpdates?timeout{}"
        if self.last:
            # offset from last handled update
            url += "&offset={}"
            offset = self.last + 1
        return url.format(CONFIG.bot()["token"], self.timeout, offset)

    @property
    def last(self):
        """Return the identifier of the last handled update"""

        return self.__updates["last"]

    @property
    def timeout(self):
        """Return active polling timeout value"""

        return self.__updates["timeout"]

    @timeout.setter
    def timeout(self, value):
        """Change polling timeout value"""

        self.__updates["timeout"] = value

    def queue(self):
        """Return number of updates in queue"""

        return len(self.__updates["queue"])


if __name__ == "__main__":

    # Start configuration & database managers
    updates = UpdatesHandler()

    with DBMANAGER as managing:
        while managing.ready:
            # recieve updates
            updates.checkoff()
            COMMANDS.messenger.deliver()
            time.sleep(1)
