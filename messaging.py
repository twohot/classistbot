"""Classes & Methods used by Class Assistant Bot when responding to
Telegram Updates/messages"""

# import json
import urllib
import requests
from storing import CONFIG

URL_PREFIX = "https://api.telegram.org/bot{}/"


MSG_DEFAULTS = {
    "text": {
        "method": "sendMessage?",
        "json": {
            "chat_id": 0,
            "text": "",
            "parse_mode": "",
            "entities": [],
            "disable_web_page_preview": 0,
            "disable_notification": 0,
            "reply_to_message_id": 0,
            "allow_sending_without_reply": 0,
            "reply_markup": {},
        },
    },
    "sticker": {
        "method": "sendSticker?",
        "json": {
            "chat_id": 0,
            "sticker": "",  # file_id as string or URL
            "disable_notification": 0,
            "reply_to_message_id": 0,
            "allow_sending_without_reply": 0,
            "reply_markup": {},
        },
    },
    "delete": {
        "method": "deleteMessage?",
        "json": {
            "chat_id": 0,
            "message_id": 0,
        },
    },
}

MSG_RIGHTS = {"TUTOR", "REPRESENTATIVE", "STUDENT", "FACULTY"}


class MessageHeader:
    """Header of a Message"""

    def __init__(self, values):
        """initialize this Header from a tuple/list"""

        self.__header = {
            "date": 0,
            "recipient": 0,
            "reference": 0,
            "sender": 0,
            "privilege": "STUDENT",
            "scene": "private",
        }
        (
            self.date,
            self.recipient,
            self.reference,
            self.sender,
            self.__header["scene"],
        ) = values

    @property
    def recipient(self):
        """Returns the identifier of the recipient"""

        return self.__header["recipient"]

    @recipient.setter
    def recipient(self, value):
        """Sets the identifier of the recipient"""

        if isinstance(value, int):
            self.__header["recipient"] = value

    @property
    def reference(self):
        """Returns the identifier of the reference message"""

        return self.__header["reference"]

    @reference.setter
    def reference(self, value):
        """Sets the identifier of the reference message"""

        if isinstance(value, int):
            self.__header["reference"] = value

    @property
    def sender(self):
        """Returns the identifier of the sender"""

        return self.__header["sender"]

    @sender.setter
    def sender(self, sender_id):
        """Sets the sender identifier which also determines
        the privileges/context for responding to Updates"""

        if isinstance(sender_id, int):
            self.__header["sender"] = sender_id
            # if recipient is not human (supergroup), set privileges.
            self.privilege = self.__header["recipient"]

    @property
    def date(self):
        """Returns the date (integer) of the reference message"""

        return self.__header["date"]

    @date.setter
    def date(self, value):
        """Sets the date (integer) of the reference message"""

        if isinstance(value, int):
            self.__header["date"] = value

    @property
    def privilege(self):
        """Returns the privilege/authority of the Message"""

        return self.__header["privilege"]

    @privilege.setter
    def privilege(self, group_id):
        """Set the privilege of the Message based on the provided Group.
        A Group and the roles of the discussants within the group provides
        context for privileges.  A course code can double as a group."""

        privilege_is_set = False
        school = CONFIG.school()
        configured_roles = MSG_RIGHTS.intersection(school)
        for role in configured_roles:
            for user_id in school[role]:
                for group in school[role][user_id]:
                    if group_id in group and self.__header["sender"] == user_id:
                        self.__header["privilege"] = role
                        privilege_is_set = True
                        break
                if privilege_is_set:
                    break
            if privilege_is_set:
                break

    @property
    def scene(self):
        """Returns the scene/setting of the correspondence
        e.g. a supergroup, or private chat"""

        return self.__header["scene"]


class MessageStates:
    """Status of a Messenger -- Edited or Not"""

    def __init__(self):
        """Initialize the status"""

        self.__states = {
            "edited": False,
            "resolves": None,
        }

    @property
    def edited(self):
        """Return True for an edited Message"""

        return self.__states["edited"]

    @edited.setter
    def edited(self, boolean):
        """Set the edit status of a Message"""

        if isinstance(boolean, bool):
            self.__states["edited"] = boolean

    @property
    def resolves(self):
        """Return Identifier of an awaited message"""

        return self.__states["resolves"]

    @resolves.setter
    def resolves(self, value):
        """Set Identifier of an awaited Message"""

        if isinstance(value, int):
            self.__states["resolves"] = value


class MessageProtocol:
    """The response protocol object"""

    def __init__(self, msg_t):
        """Initialize the response template"""

        self.__protocol = {
            "method": "",
            "type": "",
            "template": dict(),
        }
        self.type = msg_t

    @property
    def type(self):
        """Return the type of this Message"""

        return self.__protocol["type"]

    @type.setter
    def type(self, msg_t):
        """Set the type of this Message"""

        if msg_t in MSG_DEFAULTS:
            self.__protocol["type"] = msg_t
            self.__protocol["method"] = MSG_DEFAULTS[msg_t]["method"]
            self.__protocol["template"] = dict()
            self.__protocol["template"].update(MSG_DEFAULTS[msg_t]["json"])

    @property
    def template(self):
        """Return a copy of the template"""

        return self.__protocol["template"]

    @property
    def method(self):
        """Return the URL Query method"""

        return self.__protocol["method"]


class Message:
    """The Telegram Message Object"""

    def __init__(self, headertuple, mode="text", value=""):
        """Initialize message from a header-tuple, default mode & content"""

        self.__message = {
            "header": MessageHeader(headertuple),
            "protocol": MessageProtocol(mode),
            "json": dict(),  # actual message to be sent
            "states": MessageStates(),
        }
        self.__message["json"].update(MSG_DEFAULTS[mode]["json"])
        self.outbound = {"chat_id": self.header.recipient}
        self.content = value

    def __call__(self):
        """Return the Telegram message json"""

        return self.__message["json"]

    @property
    def header(self):
        """Return the header object of this Message"""

        return self.__message["header"]

    @property
    def protocol(self):
        """Return the protocol object of this Message"""

        return self.__message["protocol"]

    @protocol.setter
    def protocol(self, msg_t):
        """Set the messaging protocol"""

        if msg_t in MSG_DEFAULTS:
            self.__message["protocol"] = MessageProtocol(msg_t)
            # Determine excess due to change of type
            drop = set.difference(
                set(self.__message["json"]), set(MSG_DEFAULTS[msg_t]["json"])
            )
            # Determine deficiency due to change of type
            pick = set.difference(
                set(MSG_DEFAULTS[msg_t]["json"]), set(self.__message["json"])
            )
            for key in drop:
                del self.__message["json"][key]
            for key in pick:
                self.__message["json"][key] = MSG_DEFAULTS[msg_t]["json"][key]

    @property
    def reply_to(self):
        """Return the Id of message being replied"""

        return self.__message["json"]["reply_to_message_id"]

    @reply_to.setter
    def reply_to(self, value):
        """Set Id of message being replied"""

        if isinstance(value, int):
            self.__message["json"]["reply_to_message_id"] = value

    @property
    def content(self):
        """Return textual content of this message"""

        return self.__message["json"]["text"]

    @content.setter
    def content(self, value):
        """Set the textual content of this message"""

        if isinstance(
            value,
            (
                str,
                list,
            ),
        ):
            # uses list when conveying commands for processing
            self.__message["json"]["text"] = value

    @property
    def states(self):
        """Return the state of this message"""

        return self.__message["states"]

    @property
    def outbound(self):
        """Return the outbound json message"""

        return self.__message["json"]

    @outbound.setter
    def outbound(self, argdict):
        """Update the outbound json message from a dictionary"""

        for key, value in argdict.items():
            if key in self.__message["json"] and isinstance(
                value, type(self.__message["json"][key])
            ):
                self.__message["json"][key] = value


class MessengerStates:
    """Status of a Messenger"""

    def __init__(self):
        """Initialize the status"""

        self.__state = {
            "awaiting": [],
            "recent": {},
        }

    def __refresh__(self):
        """Removes recorded command events that are old"""

        for command in self.__state["recent"]:
            # TODO: Clean up
            print(command)

    @property
    def awaiting(self):
        """Return expected follow-up identifiers"""

        return self.__state["awaiting"]

    @awaiting.setter
    def awaiting(self, value):
        """Set the expected follow-up identifier"""

        if isinstance(value, int) and value not in self.__state["awaiting"]:
            self.__state["awaiting"].append(value)

    @property
    def recent(self):
        """Return list of recently executed commands"""

        self.__refresh__()
        return self.__state["recent"].keys()

    @recent.setter
    def recent(self, values):
        """Set a recently executed command from tuple
        tuple example: ('command', 'time-of-execution')"""

        if isinstance(values, tuple) and len(values) == 2:
            self.__state["recent"].update(values)

    def resolve(self, identifier):
        """Remove the identifier entry from awaiting list"""

        if identifier in self.__state["awaiting"]:
            index = self.__state["awaiting"].index(identifier)
            del self.__state["awaiting"][index]


class Messenger:
    """Command broker and Responder"""

    def __init__(self):
        """Initialize variables"""

        # commands requiring followup updates
        self.__messenger = {
            "states": MessengerStates(),
            "queue": [],
        }

    def queue(self, message):
        """Add a message to the messenger's queue"""

        if isinstance(message, Message):
            self.__messenger["queue"].append(message)

    def deliver(self):
        """Send response messages back to their appropriate destination"""
        
        for index, message in enumerate(self.__messenger["queue"]):
            count = 0
            url_method = message.protocol.method
            for key, value in message.outbound.items():
                if not value:
                    continue
                arg = key + "=" + urllib.parse.quote_plus(str(value))
                if count > 0:
                    arg = "&" + arg
                url_method += arg
                count += 1

            url = URL_PREFIX.format(CONFIG.bot()["token"]) + url_method
            print(message())

            # send the message and get sending status
            send_status = requests.get(url)

            if str(send_status) == "<Response [200]>":
                awaiting_resolution = message.states.resolves
                del self.__messenger["queue"][index]
                # check if this resolves an awaited post
                if awaiting_resolution in self.states.awaiting:
                    self.states.resolve(awaiting_resolution)

    @property
    def states(self):
        """Return the Messenger's States object"""

        return self.__messenger["states"]

    @property
    def message(self):
        """Return the initiating message"""

        return self.__messenger["queue"][0]

    def draft(self):
        """Draft a follow-up message"""

        when = self.message.header.date
        to_whom = self.message.header.recipient
        about = self.message.header.reference
        sent_from = self.message.header.sender
        where = self.message.header.scene

        return Message(
            (
                when,
                to_whom,
                about,
                sent_from,
                where,
            )
        )
