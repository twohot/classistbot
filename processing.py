"""Commands implemented for Class Assistant Bot"""

from storing import CONFIG, DBMANAGER
from messaging import Messenger


def next_id(table):
    """Return the next value of row-id in table"""

    cursor = DBMANAGER.connection.cursor()
    cursor.execute("""SELECT MAX(id) FROM {};""".format(table))
    id_entry = cursor.fetchall()[0]
    next_number = 1
    if not None in id_entry:
        next_number += int(id_entry[0])
    cursor.close()
    return next_number


def get_courses():
    """Return a List of all Courses/Groups registered with tho Bot"""

    cursor = DBMANAGER.connection.execute("""SELECT * FROM chat;""")
    return cursor.fetchall()


def course_data(course_code, school_id):
    """Return List representing student's registration for the
    specified course in the active session."""

    cursor = DBMANAGER.connection.execute(
        """
        SELECT
            chat.id as id
            , chat.course_code
            , course_title
            , member_id
            , surname
            , first_name
            , middle_name
        FROM chat
        JOIN register
            ON chat.course_code=register.course_code
        JOIN member
            ON member_id=member.id
            WHERE chat.course_code=?
            AND member_id=?
            AND academic_session=?;
        """,
        (
            course_code,
            school_id,
            CONFIG.school()["session"],
        ),
    )
    return cursor.fetchall()


def registered(course_code, school_id, context_message):
    """Check that the student registered the course"""

    state = False
    registration_info = course_data(course_code, school_id)
    if not registration_info:
        context_message.content = (
            "Oops! Nobody is registered for "
            + "{0} with that number!".format(course_code)
        )
        return state
    return registration_info


def member_device_data(school_id, chat_id):
    """Return list representing selected student or devices.
    Where school_id and chat_id belong to different students, list will
    have two entries."""

    cursor = DBMANAGER.connection.execute(
        """
        SELECT
            id
            , telegram_id
        FROM member
            WHERE id=? OR telegram_id=?;
        """,
        (
            school_id,
            chat_id,
        ),
    )
    return cursor.fetchall()


def course(messenger):
    """Set or Update the Course
    /course set|update value"""

    if messenger.message.header.privilege == "TUTOR":
        messenger.message.content = (
            "Sorry.  I don't know how to do this yet.\nTry again later."
        )
    else:
        messenger.message.content = "Want to teach?"


def session(messenger):
    """Set or Update the Academic Session
    /session set|update|<active> value"""

    if messenger.message.header.privilege == "TUTOR":
        messenger.message.content = (
            "Sorry.  I am still learning this command.\nTry again later."
        )
    else:
        messenger.message.content = (
            "Although it is the year 2021, "
            + "The 20192020 Academic Session is still active and running!"
        )


def member(messenger):
    """Set or update a student member in the Group Channel
    /member set|update username values"""

    if messenger.message.header.privilege == "TUTOR":
        messenger.message.content = (
            "This is a valid command but I haven't learned it.\n Sorry."
        )
    else:
        messenger.message.content = (
            "What inspired this experiment?\nBot's don suffer!"
        )


def quiz(messenger):
    """Manage Quizzes
    /quiz set|update|<list> [quiz_id] values"""

    if messenger.message.header.privilege == "TUTOR":
        messenger.message.content = (
            "Someone should teach me about quizzes. Please"
        )
    else:
        messenger.message.content = (
            "Wow! Why are students so interested in quizzes?.\n"
            + "Try again later.  I am still studying how to do quizzes."
        )


def progress(messenger):
    """View student's progress
    /progress [username]"""

    messenger.message.content = "Check back later for this feature."


def event(messenger):
    """Manage Events
    /event schedule|update|list|next|<active>"""

    messenger.message.content = (
        "Relax! Nothing is happening.\n Just stay tuned!"
    )


def readme(messenger):
    """Direct sender to the README.md documentation
    /readme"""

    messenger.message.content = (
        "https://gitlab.com/twohot/classistbot/-/blob/prime/README.md"
    )


def announce(messenger):
    """Broadcast and Pin Messages to the Group
    /announce group message"""

    if messenger.message.header.privilege in ["TUTOR", "REPRESENTATIVE"]:
        # check that sender is an admin in the said group
        # if so, target response to that group and set message
        messenger.message.content = "Sir, you haven't shown me how to do this."
    else:
        messenger.message.content = (
            "I only take such orders from Administrators."
        )


def bonus(messenger):
    """Award bonus marks to a member/student in a channel

    e.g. /bonus view|<award> 5 2017030189956 ARC496
    the above example records 5 points for the candidate with registration
    number 2017030189956 in ARC496 for the active session
    """

    if len(messenger.message.content) > 3 or len(messenger.message.content) < 2:
        messenger.message.content = "Are you sure you read the manual?"
        return

    if len(messenger.message.content) == 2:
        course_code = None
        points, school_id = messenger.message.content
        for group in get_courses():
            if group[0] == messenger.message.header.recipient:
                course_code = group[1]
                break
        if not course_code:
            messenger.message.content = (
                "I'm afraid I cannot determine the context of the award.\n"
                + "Have you considered providing a course code?"
            )
            return

    if len(messenger.message.content) == 3:
        marks, school_id, course_code = messenger.message.content
        messenger.message.header.privilege = course_code

    # Check that the student registered the course
    registration_info = registered(course_code, school_id, messenger.message)
    if not registration_info:
        return

    if messenger.message.header.privilege in ["TUTOR"]:
        group_id = registration_info[0][0]
        course_title = registration_info[0][2]
        surname = registration_info[0][4]
        firstname = registration_info[0][5]

        if marks.isdecimal() and True in [
            int(marks) <= int(CONFIG.school()["unit_bonus"])
        ]:
            new_id = next_id("bonus")
            cursor = DBMANAGER.connection.cursor()
            cursor.execute(
                """INSERT INTO bonus VALUES (?,?,?,?,?,?);""",
                (
                    new_id,
                    school_id,
                    course_code,
                    marks,
                    messenger.message.header.date,
                    CONFIG.school()["session"],
                ),
            )
            DBMANAGER.connection.commit()
            cursor.close()
            content = (
                'Yay! The Course Tutor for "{0}" ({1}) just awarded '
                + "{2} bonus mark(s) to {3} {4} ({5}).\n\n"
                + "We reward participation!"
            )
            messenger.message.outbound = {"chat_id": group_id}
            messenger.message.content = content.format(
                course_title.title(),
                course_code,
                marks,
                firstname.title(),
                surname.title(),
                school_id,
            )
            conclusion = messenger.draft()
            conclusion.content = "Bonus mark awarded successfully"
            messenger.queue(conclusion)
        else:
            messenger.message.content = (
                "I could barely understand that message.\n\n"
                + "Perhaps you've got the syntax muddled up somewhere or "
                + "you are attempting to award more than "
                + "{0} bonus ".format(CONFIG.school()["unit_bonus"])
                + "point(s) at a time."
            )
    else:
        messenger.message.content = "Only Tutors can give bonuses."


def whois(messenger):
    """Return information about a registered member.
    This should be handy for confirmation.

    e.g. /whois 2017030189956 ARC496
    Output: Names and registered Courses in the active session
    that matches the registration number or username
    """

    if len(messenger.message.content) > 2 or len(messenger.message.content) < 1:
        messenger.message.content = "Who is what?"
        return

    if len(messenger.message.content) == 1:
        course_code = None
        school_id = messenger.message.content[0]
        for group in get_courses():
            if group[0] == messenger.message.header.recipient:
                course_code = group[1]
                break
        if not course_code:
            messenger.message.content = (
                "I'm afraid I cannot determine the context of the enquiry.\n"
                + "Have you considered providing a course code?"
            )
            return

    if len(messenger.message.content) == 2:
        school_id, course_code = messenger.message.content
        messenger.message.header.privilege = course_code

    # Check that the student registered the course
    registration_info = registered(course_code, school_id, messenger.message)
    if not registration_info:
        return

    if messenger.message.header.privilege in ["TUTOR"]:
        course_title = registration_info[0][2]
        surname = registration_info[0][4]
        firstname = registration_info[0][5]
        midname = registration_info[0][6]
        content = (
            "{0} is the registration number belonging to {1}, {2} {3}.  "
            + "{2} is taking {4} ({5})."
        )
        messenger.message.content = content.format(
            school_id.title(),
            surname.title(),
            firstname.title(),
            midname.title(),
            course_code,
            course_title.title(),
        )
    else:
        messenger.message.content = (
            "That's classified, and I respect people's privacy."
        )


def register(messenger):
    """Register a student for a Telegram Course Group.

    e.g. /register ARC216 2017030189956 SURNAME FIRSTNAME MIDDLENAME PHONE
    The above should record the user into the database if not existing, then
    register the user for the ARC216 course group.
    """

    if len(messenger.message.content) != 6:
        messenger.message.content = (
            "You posted a faulty request. Remember the format.\n"
            + "Here are some examples:\n\n"
            + "/register ARC444 ESUT/2014/150333 OKORO GABRIEL NCHAOMA +234803890988\n"
            + "/register GST221 2017030189956 AKUNNA BRENDA CHINYERE +234703139023"
        )
        return

    (
        course_code,
        school_id,
        surname,
        first_name,
        middle_name,
        phone,
    ) = messenger.message.content

    if not phone.startswith("+"):
        messenger.message.content = (
            "I don't see any Country code in the mobile number you provided. "
            + "Please try again with a properly formatted phone number"
        )
        return

    # confirm that either student or device is new
    registration_info = member_device_data(
        school_id, messenger.message.header.sender
    )

    if len(registration_info) > 1:
        # member exists with another device or vice-versa
        messenger.message.content = (
            "Hmm! It seems that someone with a different Telegram account "
            + "has registered with that device before you, ... or you had "
            + "registered before with another device.  In any case, you "
            + "should never share devices for your course work.\n\n"
            + "Your tutor will surely hear about this."
        )
        # TODO: Add report Message to Messenger queue
        return

    if len(registration_info) == 1:
        reg_school_id, reg_chat_id = registration_info[0]

        if (
            reg_school_id == school_id
            and reg_chat_id == messenger.message.header.sender
        ):
            # member owns the device.
            messenger.message.content = (
                "I can confirm that you are on our records already.\n Now let's "
                + "look into the registration for {0}.".format(course_code)
            )
        else:
            # Someone is attempting registration by proxy.  Stop fraud!
            messenger.message.content = (
                "What you are trying to do is wrong!\n You should never use "
                + "your device to register for someone.  No one should ever "
                + "use their device to register for you either.\n\n"
                + "I will report this fraudulent attempt to your tutor."
            )
            # TODO: Add report Message to Messenger queue
            return

    else:
        # Completely new registration for both device and student.
        cursor = DBMANAGER.connection.cursor()
        cursor.execute(
            """INSERT INTO member VALUES (?,?,?,?,?,?);""",
            (
                school_id,
                messenger.message.header.sender,
                surname.upper(),
                first_name.upper(),
                middle_name.upper(),
                phone,
            ),
        )
        DBMANAGER.connection.commit()
        cursor.close()
        messenger.message.content = (
            "You are now on my records.  Congrats! ...\nWait! It's not over "
            + "yet.  Now processing you for {0}.".format(course_code)
        )

    course_message = messenger.draft()
    # confirm that student is registering course for the first time.
    course_info = registered(course_code, school_id, course_message)

    if course_info:
        # Already registered the course
        course_message.content = (
            "You mean you went through all that trouble just to confirm that "
            + "you are registered already?  Now you know ... You were "
            + "registered for {0}!  Happy now?".format(course_code)
        )
        messenger.queue(course_message)
    else:
        # New registration for the course
        messenger.queue(course_message)
        new_id = next_id("register")
        cursor = DBMANAGER.connection.cursor()
        cursor.execute(
            """INSERT INTO register VALUES (?,?,?,?);""",
            (
                new_id,
                school_id,
                course_code.upper(),
                CONFIG.school()["session"],
            ),
        )
        DBMANAGER.connection.commit()
        cursor.close()
        course_message.content = (
            "It's DONE!  You can now get scores for "
            + "{0} while participating in the relevant ".format(course_code)
            + "group on Telegram.  Cheers!"
        )


def configure(messenger):
    """Add or Update the value of a configuration parameters

    format: /set <parameter> <value>
    example: /set unit_bonus 1"""

    messenger.message.content = "This feature is still on the drawing table."


# available commands
class CommandsManager:
    """Process Commands in Messages"""

    def __init__(self, messenger):
        """Initialize Manager with a message"""

        self.__commands = {
            "commands": {
                "/course": course,
                "/session": session,
                "/session@classistbot": session,
                "/member": member,
                "/quiz": quiz,
                "/quiz@classistbot": quiz,
                "/progress": progress,
                "/event": event,
                "/event@classistbot": event,
                "/announce": announce,
                "/readme": readme,
                "/readme@classistbot": readme,
                "/bonus": bonus,
                "/whois": whois,
                "/register": register,
                "/set": configure,
            },
            "messenger": None,
            "issued": {},
        }
        self.messenger = messenger

    @property
    def messenger(self):
        """Return a Messenger object"""

        return self.__commands["messenger"]

    @messenger.setter
    def messenger(self, msgr):
        """Welcomes (set) the Messenger for responses"""

        if isinstance(msgr, Messenger):
            self.__commands["messenger"] = msgr

    def remove(self, privmsg):
        """Remove a message with private notice"""

        self.messenger.message.protocol = "delete"
        self.messenger.message.outbound = {
            "message_id": self.messenger.message.header.reference
        }
        notify = self.messenger.draft()
        notify.outbound = {"chat_id": self.messenger.message.header.sender}
        notify.content = privmsg
        self.messenger.queue(notify)

    def execute(self, command):
        """Run command from messenger"""

        if (
            CONFIG.bot()["enforce_privacy"]
            and command.find("@classistbot") == -1
            and self.messenger.message.header.scene == "supergroup"
        ):
            # private command issued in public
            self.remove(
                "Please issue 'private queries' privately.\n"
                + "I am here.  Use private commands here. If a command is "
                + "not public, do not use it where everyone will see."
            )
        else:
            try:
                if (
                    command in self.__commands["issued"]
                    and self.messenger.message.header.scene == "supergroup"
                    and (
                        self.messenger.message.header.date
                        - self.__commands["issued"][command]
                    )
                    <= int(CONFIG.bot()["abuse_delay"])
                ):
                    self.remove(
                        "{} was issued a while ago. ".format(command)
                        + "Why the Noise?"
                    )
                    return
                self.__commands["commands"][command](self.messenger)
                self.__commands["issued"][
                    command
                ] = self.messenger.message.header.date
            except KeyError:
                # Command not implemented
                self.messenger.message.content = "I don't know that command"
