"""Classes for Creating and Managing Quizzes"""


OPTION_TYPES = {
    "boolean": (2, 2),
    "single" : (4, 5),
    "multiple" : (5, 10),
}


class Media:
    """A media object"""

    def __init__(self, identifier):
        """Initialize media instance"""

        self.__media = {
            "id": None,
            "type" : "image",
            "content" : None,
        }

        if isinstance(identifier, int):
            self.__option["id"] = identifier
        else:
            raise TypeError("Bad Identifier value")


    def add(self):
        """Add a media of active type"""

    def remove(self):
        """Remove media"""

        self.__media["content"] = None

    @property
    def type(self):
        """Return the type of media"""

        self.__media["type"]

    @type.setter
    def type(self, value):
        """Set the type of Media"""



class Option:
    """An answer option"""

    def __init__(self, identifier):
        """Initialize Option instance"""

        self.__option = {
            "id": None,
            "text":None,
            "media":None,
        }

        if isinstance(identifier, (int, float, str,)):
            self.__option["id"] = identifier
        else:
            raise TypeError("Bad Identifier value")

    def add(self, value, optiontype="text"):
        """Add to this option"""

        if optiontype == "text" and isinstance(value, str):
            self.__option["text"] = value
        elif optiontype == "media" and isinstance(value, Media):
                self.__option["media"] = value
        else:
            print("ERROR:\tAttempt to assign wrong type or value")

    def remove(self, optiontype):
        """Remove from this option"""

        if optiontype in self.__option:
            self.__option[optiontype] = None

    @property
    def media(self):
        """Return this Option's Media Object"""

        return self.__option["media"]

    @property
    def text(self):
        """Return this Option's text value"""

        return self.__option["text"]



class OptionsGroup:
    """A Group of options"""

    def __init__(self, optiontype="single", optslist=None):
        """Initialize options"""
        
        self.__group = {
            "options": None,
            "type": "single",
            "correct": None,
        }

        self.type = optiontype
        self.options = optslist
    
    @property
    def options(self):
        """Return a Dict of Options"""

        return self.__group["options"]

    @options.setter
    def options(self, optslist):
        """Set options from a List.
        Entries preceded by a tilde are correct"""

        new_options = list()
        for opt in optslist:
            if not isinstance(opt, Option):
                print("WARN::\tCould not update Options -- Wrong values")
                return
            new_options.append(opt)
        self.__group["options"] = new_options

    @property
    def type(self):
        """Return this Option's type"""
        
        return self.__group["type"]

    @type.setter
    def type(self, value):
        """Set this Options Group type"""

    def add(self, opt):
        """Add an Option to the group"""

        if isinstance(opt, Option):
            self.__group["options"].append(opt)

    def remove(self, tag):
        """Remove the option with the specified tag"""




class Question:
    """A question in a Quiz"""

    #class instance tagging
    lastsaved = 0
    lastinstance = 0

    def __init__(self, text, options):
        """initialize this question"""
    
        self.__attr = {}
        self.text = text
        self.options = options
        
        self.identifier = cls.next_id()

    @property
    def text(self):
        """Get this Question"""

        return self.__attr['text']

    @text.setter
    def text(self, value):
        """Set this Question"""

        self.__attr['text'] = value

    @classmethod
    def (cls):
        """Increment class instance counter and return result"""

        cls.next_id += 1

        #self.max_options = 0        #-Number of options allowed
        #self.options = []
        #self.identifier = 0         #-Id number
        #self.mark = 0               #-Score point
        #self.selection = []
        #self.random = False
        #self.image = None



class Quiz:
    """A Quiz"""

    #self.identifier = 0         #-Id number
    #self.start = 0              #-Date/time
    #self.duration = 0           #-time
    #self.title = ""             #-Title of the Quiz
    #self.course = None          #-course code
    #self.random = False
    #self.marks = 0              #-Total marks
    #self.questions = []         #-List of Questions
