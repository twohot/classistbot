"""Database Methods for Class Asistant Bot"""

import os
import sqlite3


DB_TABLES = {"admin", "chat", "config", "member", "register", "role", "bonus"}


def config():
    """Find prefered configuration file, parse it
    Returns -> Dict of the configuration."""

    conf = {}
    home = os.environ.get("HOME")

    # Possible config file paths
    for path in [
        home + "/.classistbot/config",
        home + "/.config/classistbot/config",
        "/etc/classistbot/config",
    ]:
        # Use first config file that exists
        if os.path.exists(path):
            with open(path, "r") as config_file:
                for setting in list(config_file):
                    if not setting.strip() or setting.startswith("#"):
                        continue
                    key, value = setting.strip().split("=")
                    if value.strip().lower() in ["yes", "1", "true"]:
                        value = True
                    elif value.strip().lower() in ["no", "0", "false"]:
                        value = False
                    conf[key] = value
            break
    if "database" in conf.keys():
        if not os.path.exists(conf["database"]):
            # Use Default database file
            conf["database"] = home + "/.classistbot.db"
    return conf


class Settings:
    """Configuration Manager Object"""

    def __init__(self, configdict):
        """Read configuration file and initialize basic
        settings from it."""

        self.__bot = {}
        self.__school = {}
        for key, value in configdict.items():
            self.__bot[key] = value

    def bot(self):
        """Returns a Dict of settings specific to bot"""

        return self.__bot

    def school(self):
        """Returns a Dict of setting specific to schooling"""

        return self.__school

    def update(self, key, value):
        """Change the value of a School/Faculty setting"""

        if key in self.__school.keys():
            self.__school[key] = value

    def fetch(self, sqlfetch, conftype):
        """grab school configuration from database"""

        if conftype == "school":
            self.__school.update(sqlfetch)
        elif conftype == "group_admins":
            for role, user_id, group_id, course_code in sqlfetch:
                if role not in self.__school:
                    self.__school[role] = {}
                if user_id not in self.__school[role]:
                    self.__school[role][user_id] = []
                self.__school[role][user_id].append(
                    (
                        group_id,
                        course_code,
                    )
                )


class DatabaseManager:
    """Database Manager Object"""

    def __init__(self, configurations):
        """Connect to database and load basics"""

        # tables that must exist in database
        self.__manager = {
            "missing": {},
            "ready": False,
            "connection": None,
            "settings": None,
        }

        if isinstance(configurations, Settings):
            self.__manager["settings"] = configurations
        else:
            raise TypeError(
                "Expected a 'Settings' Object, got {}".format(
                    type(configurations)
                )
            )

    def __enter__(self):
        """Return the database manager as file-handle"""

        self.__manager["connection"] = sqlite3.connect(
            self.__manager["settings"].bot()["database"]
        )
        self.check()
        if not self.ready:
            raise sqlite3.OperationalError("Incomplete Database Tables")
        self.group_settings()
        return self

    def __exit__(self, error_type, error_value, error_traceback):
        """Close open connection/cursor before going out of context"""

        print("Exiting DatabaseManager context....")
        print(
            f"Type: {error_type}, Value: {error_value}, Traceback: {error_traceback}"
        )
        self.__manager["connection"].close()

    @property
    def connection(self):
        """Return database connection"""

        return self.__manager["connection"]

    def group_settings(self):
        """load settings partaining to Course Groups from Database"""

        # Get group/live configuration
        cursor = self.__manager["connection"].cursor()
        cursor.execute("""SELECT * FROM config;""")
        self.__manager["settings"].fetch(cursor.fetchall(), "school")

        # Get Tutors and Representatives
        cursor.execute(
            """
            SELECT
                name,
                telegram_id,
                chat_id,
                course_code
            FROM admin
            JOIN role
                ON role_id=role.id
            JOIN member
                ON member_id=member.id
            JOIN chat
                ON chat_id=chat.id;"""
        )
        self.__manager["settings"].fetch(cursor.fetchall(), "group_admins")
        cursor.close()

    def missing(self):
        """Return missing tables"""

        return self.__manager["missing"]

    @property
    def ready(self):
        """Return the state of the Manager"""

        return self.__manager["ready"]

    def check(self):
        """Confirm presence of critical tables"""

        cursor = self.__manager["connection"].cursor()
        cursor.execute(
            """
            SELECT
                name
            FROM sqlite_master
            WHERE type='table';"""
        )
        tables = {col[0] for col in cursor.fetchall()}
        cursor.close()
        self.__manager["missing"] = set.difference(DB_TABLES, tables)
        if tables and not self.__manager["missing"]:
            self.__manager["ready"] = True
        else:
            self.__manager["ready"] = False
            print(
                "The following Tables are absent in the database: {}".format(
                    self.__manager["missing"]
                )
            )
        return self.__manager["ready"]


README = "https://gitlab.com/twohot/classistbot/-/tree/prime"
CONFIG = Settings(config())
DBMANAGER = DatabaseManager(CONFIG)
